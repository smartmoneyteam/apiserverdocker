var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var iban = require('../aux/iban');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var VerifyUserAccessToken = require('../auth/VerifyUserAccessToken');
var Account = require('./Account');
var ObjectId = require('mongoose').Types.ObjectId;

// CREATES A NEW ACCOUNT FOR AN USER

router.post('/', VerifyUserAccessToken, function (req, res) {
    console.log('POST /accounts ' + req.params.user_id);
    Account.create({
        iban: iban.iban(),
        balance: 0,
        user_id: new ObjectId(req.params.user_id) 
    }, function (err, account) {
        if (err) return res.status(500).send('There was a problem adding the information -account- to the database.');
        res.status(200).send(account);
        console.log(' Account created: ' + account.iban);
    });
});

// RETURNS ALL THE ACCOUNTS OF THE USER
router.get('/', VerifyUserAccessToken, function (req, res) {
    console.log('GET /accounts ' + req.params.user_id);
    var query = { user_id: new ObjectId(req.params.user_id) };
    var params = 'iban user_id balance';
    Account.find(query, params,function (err, accounts) {
        if (err) return res.status(500).send('There was a problem finding the accounts.');
        res.status(200).send(accounts);
    });
});

// GETS A SINGLE ACCOUNT FROM THE DATABASE
router.get('/:id', VerifyUserAccessToken, function (req, res) {
    console.log('GET /accounts/' + req.params.id + ' ' + req.params.user_id);
    var query = { _id: new ObjectId(req.params.id), user_id: new ObjectId(req.params.user_id) };
    Account.find(query, function (err, accounts) {
        if (err) return res.status(500).send('There was a problem finding the account.');
        if (!accounts) return res.status(404).send('Not allowed to get this information.');
        res.status(200).send(accounts[0]);
        console.log('Done: GET Account ' + req.params.id);
    });
});

// GETS THE BALANCE OF AN ACCOUNT FROM THE DATABASE
router.get('/:id/balance', VerifyUserAccessToken, function (req, res) {
    console.log('GET /accounts/' + req.params.id + '/balance ' + req.params.user_id);
    var query = { _id: new ObjectId(req.params.id), user_id: new ObjectId(req.params.user_id) };
    //var params = 'iban user_id balance';
    Account.find(query, function (err, accounts) {
        if (err) return res.status(500).send('There was a problem finding the account.');
        if (!accounts || accounts.length==0) return res.status(404).send('Not allowed to get this information.');
        console.log(accounts[0].balance);
        res.status(200).send(''+accounts[0].balance);
        console.log('Done: GET Account balance ' + req.params.id + ' = ' + accounts[0].balance);
    });
});

// GET MOVEMENTS FROM AN ACCOUNT
router.get('/:id/movements', VerifyUserAccessToken, function (req, res) {
    console.log('GET /movements ' + req.params.id);
    var query = { _id: new ObjectId(req.params.id), user_id: new ObjectId(req.params.user_id) };
    Account.find(query, function (err, accounts) {
        if (err) return res.status(500).send('There was a problem finding the movements.');
        console.log('Done: GET Movements ' + req.params.id);
        if (accounts.length != 1) return res.status(500).send('There was a problem finding the account.');
        //return movements ordered by date
        return res.status(200).send(accounts[0].movements.sort(function(a,b){return b.creation_date.getTime() - a.creation_date.getTime()})); 
    });
});

// DELETES A ACCOUNT FROM THE DATABASE
router.delete('/:id', VerifyUserAccessToken, function (req, res) {
    //TODO: Poner seguridad
    //TODO: Comprobar que la cuenta está a 0
    //TODO: ¿Poner marca de borrado? ¿Meterla en una bdd de deleted_accounts antes de borrarla? 
    Account.findByIdAndRemove(req.params.id, function (err, account) {
        if (err) return res.status(500).send('There was a problem deleting the account.');
        res.status(200).send('Account: ' + account.iban + ' was deleted.');
    });
});


// UPDATES A SINGLE ACCOUNT IN THE DATABASE
router.put('/:id', VerifyUserAccessToken, function (req, res) {
    //TODO: Poner seguridad
    Account.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, account) {
        if (err) return res.status(500).send('There was a problem updating the account.');
        res.status(200).send(account);
    });
});

// ADDS A MOVEMENT TO ONE ACCOUNT
router.put('/:id/movements', VerifyUserAccessToken, function (req, res) {
    console.log('PUT /movements ' + req.params.id);
    var type = req.body.type;
    var desc = req.body.description;
    var amount = req.body.amount;
    // TODO: Validar parámetros de entrada
    var newMovement = {
        user_id: new ObjectId(req.params.user_id),
        type: type,
        description: desc,
        amount: amount,
        prev_balance: 0
    };
    var query = { _id: new ObjectId(req.params.id), user_id: new ObjectId(req.params.user_id) };
    Account.findOne( query, function (err, account) {
        if (err) {
            console.log(err);
            return res.status(500).send('There was a problem updating the account.' );
        }
        if (!account) return res.status(404).send('No account found.');
        newMovement.prev_balance = account.balance;
        var newBalance = account.balance + newMovement.amount;
        //Comprobar que tiene saldo
        if (newBalance < 0) return res.status(401).send('Balance not enough.');
        account.movements.push(newMovement);
        account.balance = newBalance;
        account.save();  
        console.log('PUT /movements OK:' + newBalance);
        res.status(200).send(newMovement);
    });
});

// TRANSFER TO OTHER ACCOUNT
router.put('/:id/movements/:iban', VerifyUserAccessToken, function (req, res) {
    console.log('PUT /movements FROM:' + req.params.id + ' TO ' + req.params.iban);
    var type = req.body.type;
    var desc = req.body.description;
    var amount = Math.abs(req.body.amount);
    // TODO: Validar parámetros de entrada
    var originMovement = {
        user_id: new ObjectId(req.params.user_id),
        type: type,
        description: desc,
        amount: amount * (-1),
        prev_balance: 0
    };
    var destinationMovement = {
        user_id: new ObjectId(req.params.user_id),
        type: type,
        description: desc,
        amount: amount,
        prev_balance: 0
    };

    var query2 = { iban: req.params.iban };
    // Check iban (destination account)
    Account.findOne( query2, function (err2, destinationAccount) {
        if (err2) {
            console.log(err2);
            return res.status(500).send('There was a problem connecting with the bank.' );
        }
        if (!destinationAccount) return res.status(404).send('Destination IBAN does not exist.');
        
        var query = { _id: new ObjectId(req.params.id), user_id: new ObjectId(req.params.user_id) };
        Account.findOne( query, function (err, originAccount) {
            if (err) {
                console.log(err);
                return res.status(500).send('There was a problem connecting with the bank.' );
            }
            if (!originAccount) return res.status(404).send('Error in your account id.');
            originMovement.prev_balance = originAccount.balance;
            var newBalance = originAccount.balance + originMovement.amount;
            //Comprobar que tiene saldo
            if (newBalance < 0) return res.status(401).send('Balance not enough.');
            originAccount.movements.push(originMovement);
            originAccount.balance = newBalance;
            originAccount.save();  
            console.log('PUT /movements ORIGIN OK:' + newBalance);
            destinationMovement.prev_balance = destinationAccount.balance;
            var newDestBalance = destinationAccount.balance + destinationMovement.amount;
            destinationAccount.movements.push(destinationMovement);
            destinationAccount.balance = newDestBalance;
            destinationAccount.save();  
            console.log('PUT /movements DESTINATION OK:' + newDestBalance);
            res.status(200).send(destinationMovement);    
        });

    });

});

module.exports = router;