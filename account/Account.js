var mongoose = require('mongoose');

var MovementSchema = new mongoose.Schema({
    creation_date: { type: Date, default: Date.now },
    user_id: mongoose.Schema.Types.ObjectId,
    type: String,
    description: String,
    amount: Number,
    prev_balance: Number
}, {
    versionKey: false
});

var AccountSchema = new mongoose.Schema({
    iban: String,
    creation_date: { type: Date, default: Date.now },
    user_id: mongoose.Schema.Types.ObjectId,
    balance: Number,
    movements: [MovementSchema],
}, {
    versionKey: false
});

mongoose.model('Movement', MovementSchema);
mongoose.model('Account', AccountSchema);

module.exports = mongoose.model('Movement');
module.exports = mongoose.model('Account');

