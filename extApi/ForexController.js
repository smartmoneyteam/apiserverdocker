var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var VerifyUserAccessToken = require('../auth/VerifyUserAccessToken');

const requestJson = require("request-json");
const baseAlphaURL = "https://www.alphavantage.co/";
const apiKey = "apikey=" + process.env.FOREXAPITOKEN;


router.get('/', VerifyUserAccessToken, function (req, res) {
    console.log("Invocado GET /stock");
    var httpClient = requestJson.createClient(baseAlphaURL);
    console.log("Client created");
    var query = "query?function=CURRENCY_EXCHANGE_RATE&from_currency="+ req.query.currency1 + "&to_currency="+ req.query.currency2 + "&" + apiKey;
    httpClient.get(query,
      function (err, resApi, body) {
        var response = !err ? body : {
          "msg": "Error obteniendo datos del api externo"
        };
        res.status(200).send(response);
      }
    );
});

module.exports = router;