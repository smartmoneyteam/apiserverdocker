var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var User = require('../user/User');
require('dotenv').config();
var Account = require('../account/Account');
var iban = require('../aux/iban');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

// AuthController.js
// var VerifyToken = require('./VerifyToken');

// router.get('/me2', VerifyToken, function (req, res, next) {
//     User.findById(req.userId, { password: 0 }, function (err, user) {
//         if (err) return res.status(500).send("There was a problem finding the user.");
//         if (!user) return res.status(404).send("No user found.");

//         res.status(200).send(user);
//     });
// });

router.post('/register', function (req, res) {
    console.log('POST /register ' + req.body.email);
    //Verificar que el email no existe en la BDD de usuarios
    User.findOne({ email: req.body.email }, function (err, email_user) {
        if (err) return res.status(500).send('There was a problem registering the user.');
        console.log(email_user);
        if (email_user){ 
            return res.status(400).send('Email already registered.');
        } else if (req.body.password) {
            var hashedPassword = bcrypt.hashSync(req.body.password, 10);
            User.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: hashedPassword
            }, function (err, user) {
                if (err) return res.status(500).send('There was a problem registering the user.');
                // create a token
                var token = jwt.sign({ id: user._id }, process.env.TOKENSECRET, {
                    expiresIn: 60 * 60 * 24 // expires in 24 hours
                });
                // Create a new account for user
                var new_iban = iban.iban();
                Account.create({
                    iban: new_iban,
                    user_id: user._id,
                    balance: 0
                }, function (err, account) {
                    if (err) return res.status(500).send('There was a problem adding the information -account- to the database.');
                    console.log(account);
                });
                res.status(200).send({ auth: true, token: token, id: user._id });
            });
        } else {
            return res.status(404).send('Password needed.');
        }
        console.log('POST /register ' + res.statusCode);
    });
});

// router.post('/registerSec', VerifyToken, function (req, res) {

//     var hashedPassword = bcrypt.hashSync(req.body.password, 8);

//     User.create({
//         first_name: req.body.first_name,
//         last_name: req.body.last_name,
//         email: req.body.email,
//         password: hashedPassword
//     },
//         function (err, user) {
//             if (err) return res.status(500).send("There was a problem registering the user.")
//             // create a token
//             var token = jwt.sign({ id: user._id }, process.env.TOKENSECRET, {
//                 expiresIn: 86400 // expires in 24 hours
//             });
//             res.status(200).send({ auth: true, token: token });
//         });
// });

router.post('/login', function (req, res) {
    console.log('POST /login '+ req.body.email);
    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            return res.status(500).send('Error on the server.');
        } else {
            if ((!user) || (!bcrypt.compareSync(req.body.password, user.password))) {
                return res.status(401).send('Invalid user or password');
                //Same mesage to avoid user enumeration attack
            } else {
                var token = jwt.sign({ id: user._id }, process.env.TOKENSECRET, {
                    expiresIn: 60 * 60 * 24 // expires in 24 hours
                });
                user.logged = true;
                user.save();
                res.status(200).send({ auth: true, token: token, id: user._id });
            }
        }
    });
    console.log('POST /login '+ res.statusCode);
});

// router.get('/me1', function (req, res) {
//     var token = req.headers['x-access-token'];
//     if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

//     jwt.verify(token, process.env.TOKENSECRET, function (err, decoded) {
//         if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

//         User.findById(decoded.id,
//             { password: 0 }, // projection
//             function (err, user) {
//                 if (err) return res.status(500).send("There was a problem finding the user.");
//                 if (!user) return res.status(404).send("No user found.");
//                 // res.status(200).send(user); Comment this out!
//                 next(user); // add this line
//             });
//     });

//     // add the middleware function
//     router.use(function (user, req, res, next) {
//         res.status(200).send(user);
//     });
// });


router.post('/logout', function (req, res) {
    console.log('POST /logout ' );
    var token = req.headers['x-access-token'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    } else {
        jwt.verify(token, process.env.TOKENSECRET, function (err, decoded) {
            if (err) {
                return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            } else {
                User.findById(decoded.id, function (err, user) {
                    if (err) {
                        return res.status(500).send({ auth: false, message: 'There was a problem finding the user.'});
                    } else if (!user) {
                        return res.status(401).send({ auth: false, message: 'Invalid token.'});
                    } else if (!user.logged){
                        return res.status(401).send({ auth: false, message: 'User is not logged.'});
                    } else {
                        user.logged = false;
                        user.save();
                        res.status(200).send({ auth: true, message: 'Logout successfull'}); 
                    }
                });
            }
        });
    } 
    console.log('POST /logout '+ res.statusCode);  
});
    
module.exports = router;