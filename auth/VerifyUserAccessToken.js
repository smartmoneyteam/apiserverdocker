var jwt = require('jsonwebtoken');
require('dotenv').config();


function verifyUserAccessToken(req, res, next) {
    var token = req.headers['x-access-token'];
    //console.log(req.headers);
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided.' });

    jwt.verify(token, process.env.TOKENSECRET, function (err, decoded) {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        } else {
            req.params.user_id = decoded.id;
            //console.log(decoded.id);
        }
        next();
    });
}

module.exports = verifyUserAccessToken;