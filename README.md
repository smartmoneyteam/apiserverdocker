PROYECTO SMART MONEY BANK API
=========================
Descripción del proyecto
------------------------
Smart Money Bank API es un API Rest que proporciona funcionalidades de:

- Creación cuentas usuario
- Modificación de perfiles de usuario
- Login
- Acceso a cuentas bancarias
- Contratación de cuentas bancarias
- Acceso a cambios de divisas internacionales mediante un API Externo
- Ingresos y reintegros en cuentas