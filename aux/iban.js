var Account = require('../account/Account'); 

function iban (){
    console.log('Generating new iban');
    var new_iban = false;
    do {
        new_iban = 'ES' + Math.round(Math.random()*10000000000000000000);
        //Verify that new IBAN does not exist
        Account.findOne({iban:new_iban}, function(err, iban_account){
            if (iban_account) new_iban = false;
        });
    } while (!new_iban);
    return new_iban;
}

module.exports.iban = iban;
