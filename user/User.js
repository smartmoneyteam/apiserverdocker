var mongoose = require('mongoose');  
var UserSchema = new mongoose.Schema({  
    first_name: { type: String },
    last_name: { type: String },
    email: { type: String },
    password: { type: String },
    logged: { type: Boolean, default: false },
    creation_date: { type: Date, default: Date.now }
}, {
    versionKey: 'Logged included' 
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');