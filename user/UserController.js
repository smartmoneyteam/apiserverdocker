var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var User = require('./User');
var Account = require('../account/Account');
var VerifyUserAccessToken = require('../auth/VerifyUserAccessToken');

// // CREATES A NEW USER
// router.post('/', function (req, res) {
//     User.create({
//             first_name : req.body.first_name,
//             last_name : req.body.last_name,
//             email : req.body.email,
//             password : req.body.password
//         }, 
//         function (err, user) {
//             if (err) return res.status(500).send("There was a problem adding the information to the database.");
//             res.status(200).send(user);
//         });
// });
// RETURNS ALL THE USERS IN THE DATABASE
// router.get('/', function (req, res) {
//     User.find({}, function (err, users) {
//         if (err) return res.status(500).send('There was a problem finding the users.');
//         res.status(200).send(users);
//     });
// });

// GETS A SINGLE USER FROM THE DATABASE
router.get('/:id', VerifyUserAccessToken, function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) return res.status(500).send('There was a problem finding the user.');
        if (!user) return res.status(404).send('No user found.');
        res.status(200).send(user);
    });
});

//Moved to AccountController.js (GET /accounts/ + header x-access-token)
// // GETS ACCOUNTS FOR A SINGLE USER FROM THE DATABASE
//router.get('/:userid/accounts/', function (req, res) {
/*router.get('/accounts/', VerifyUserAccessToken, function (req, res) {

    console.log('entrando en iduser por account');
    //console.log(req.params.userid);
    //Nota: La función VerifyUserAccessToken deja el user_id decodificado del token en la variable requ.params.id  
    console.log(req.params.id);

    var ObjectId = require('mongoose').Types.ObjectId;
    //var query = { user_id: new ObjectId(req.params.userid) };
    var query = { user_id: new ObjectId(req.params.id) };

    Account.find(query, function (err, account) {
        if (err) return res.status(500).send('There was a problem finding the user\'s accounts.');
        if (!account || account.length == 0) return res.status(404).send('No accounts found.');
        res.status(200).send(account);
    });
});*/

// DELETES A USER FROM THE DATABASE
// TODO: CHECK IF THE USER HAS RIGHTS TO DO THAT
router.delete('/:id', VerifyUserAccessToken, function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err, user) {
        if (err) return res.status(500).send('There was a problem deleting the user.');
        res.status(200).send('User: ' + user.first_name + ' was deleted.');
    });
});

// UPDATES A SINGLE USER IN THE DATABASE
router.put('/:id', VerifyUserAccessToken, function (req, res) {
    User.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, user) {
        if (err) return res.status(500).send('There was a problem updating the user.');
        res.status(200).send(user);
    });
});


module.exports = router;