const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var userId;
var token;
//var console=Console;

// asercion es el mecanismo para probar que el 
// sistema tenga este estado cuando llamemos a esta funcion o cuando estemos en este momento concreto.
// EXPECTED Y ACTUAL

//puedo encadenar llamadas de funciones.
// describe('First test',
//     function () {
//         it('Test that Duckduckgo works', function (done) {
//             chai.request('http://www.duckduckgo.com') //esta peticion da 200 en el resultado, el test pasa
//                 //chai.request('https://developer.mozilla.org/en-US/adsfasdfas') //esta peticion da un 404, el test no pasa
//                 .get('/')//aqui podríamos usar un .post o un .del o un .put
//                 .end(
//                     function (err, res) {
//                         //en este punto iría el test (las aserciones)
//                         //console.log(res);
//                         console.log('Request has finished');
//                         console.log(err);
//                         res.should.have.status(200); //a la respuesta le encadeno el should y le digo have estado 200
//                         done(); //mocha sabe en este momento que compruebe todas las aserciones. Ahora comprueba que estos valores son los que yo espero
//                     }
//                 )
//         }

//         )
//     }

// )


describe('Test de API Usuarios',
    // function () {
    //     it('Prueba que la API de usuarios responde correctamente', function (done) {
    //         chai.request('http://localhost:4000')
    //             .get('/apitechu/v1/hello')//aqui podríamos usar un .post o un .del o un .put
    //             .end(
    //                 function (err, res) {
    //                     //en este punto iría el test (las aserciones)
    //                     //console.log(res);
    //                     console.log('Request has finished');
    //                     res.should.have.status(200);
    //                     res.body.msg.should.be.eql("Hola desde API TechU");
    //                     //en el mensaje que devuelvo pone { "msg": "Hola desde API TechU" }
    //                     //Si fuera otra cosa, sería res.body.otracosa.should.be.eq.......
    //                     done();
    //                 }
    //             )
    //     }

    //     ),
    // it('Prueba que la API devuelve una lista de usuarios correctos', function (done) {
    //     chai.request('http://localhost:3000')
    //         .get('/apitechu/v1/users')//aqui podríamos usar un .post o un .del o un .put
    //         .end(
    //             function (err, res) {
    //                 //en este punto iría el test (las aserciones)
    //                 //console.log(res);
    //                 console.log('Request has finished');
    //                 res.should.have.status(200);
    //                 res.body.users.should.be.a('array'); //en el código del api tengo objReturn.users, es como se llama el objeto del json

    //                 for (user of res.body.users) {
    //                     user.should.have.property('id');
    //                     user.should.have.property('email');
    //                     user.should.have.property('first_name');
    //                     user.should.have.property('last_name');
    //                     user.should.have.property('password');
    //                 }
    //                 done();
    //             }
    //         )
    // }
    // ),
    it('REGISTER: Prueba de register new user', function (done) {
        chai.request('http://localhost:4000')
            .post('/apitechu/v1/auth/register')//aqui podríamos usar un .post o un .del o un .put
            .type("form")
            .send({
                "first_name": "Automated",
                "last_name": "test",
                "email": "test@test.com",
                "password": "lauren"
            })
            .end(
                function (err, res) {
                    res.should.have.status(200);
                    res.body.should.have.property('auth');
                    res.body.should.have.property('token');

                    done();
                }
            )
    }
    ),
    it('LOGIN: Prueba de login del new user', function (done) {
        chai.request('http://localhost:4000')
            .post('/apitechu/v1/auth/login') //aqui podríamos usar un .post o un .del o un .put
            .type("form")
            .send({
                "email": "test@test.com",
                "password": "lauren"
            })
            .end(
                function (err, res) {
                    res.should.have.status(200);
                    res.body.should.have.property('auth');
                    res.body.should.have.property('token');
                    res.body.should.have.property('id');
                    console.log(res.body['id']);
                    console.log(res.body['token']);
                    userId = res.body['id'];
                    token = res.body['token'];
                    done();
                }
            )
    }
    ),
    it('LOGIN Prueba de login ERRONEO del new user', function (done) {
        chai.request('http://localhost:4000')
            .post('/apitechu/v1/auth/login') //aqui podríamos usar un .post o un .del o un .put
            .type("form")
            .send({
                "email": "test@test.com",
                "password": "passworderror"
            })
            .end(
                function (err, res) {
                    res.should.have.status(401);
                    done();
                }
            )
    }
    ),
    it('GET USER:ID Prueba sin token para el new user', function (done) {
        chai.request('http://localhost:4000')
            .get('/apitechu/v1/users/' + userId) //aqui podríamos usar un .post o un .del o un .put
            .end(
                function (err, res) {
                    res.should.have.status(403);
                    res.body.should.have.property('auth');
                    res.body.should.have.property('message');
                    done();
                }
            )
    }
    ),
    it('GET USER:ID Prueba CON token para el new user', function (done) {
        chai.request('http://localhost:4000')
            .get('/apitechu/v1/users/' + userId) //aqui podríamos usar un .post o un .del o un .put
            .set('x-access-token', token)
            .end(
                function (err, res) {
                    res.should.have.status(200);
                    done();
                }
            )
    }
    ),
    it('MODIFICAR USER:ID Prueba SIN token para el new user', function (done) {
        chai.request('http://localhost:4000')
            .put('/apitechu/v1/users/' + userId) //aqui podríamos usar un .post o un .del o un .put
            .end(
                function (err, res) {
                    res.should.have.status(403);
                    done();
                }
            )
    }
    ),
    it('MODIFICAR USER:ID Prueba CON token para el new user', function (done) {
        chai.request('http://localhost:4000')
            .put('/apitechu/v1/users/' + userId) //aqui podríamos usar un .post o un .del o un .put
            .set('x-access-token', token)
            .type("form")
            .send({
                "first_name": "nombreModificado"
            })
            .end(
                function (err, res) {
                    res.should.have.status(200);
                    done();
                }
            )
    }
    ),
    it('GET USER:ID Prueba CON token del nombre modificado', function (done) {
        chai.request('http://localhost:4000')
            .get('/apitechu/v1/users/' + userId) //aqui podríamos usar un .post o un .del o un .put
            .set('x-access-token', token)
            .end(
                function (err, res) {
                    res.body.first_name.should.be.eql("nombreModificado");
                    done();
                }
            )
    }
    ),
    it('DELETE USER:ID Prueba SIN token del nombre modificado', function (done) {
        chai.request('http://localhost:4000')
            .delete('/apitechu/v1/users/' + userId) //aqui podríamos usar un .post o un .del o un .put
            .end(
                function (err, res) {
                    res.should.have.status(403);                    
                    done();
                }
            )
    }
    ),
    it('DELETE USER:ID Prueba CON token del nombre modificado', function (done) {
        chai.request('http://localhost:4000')
            .delete('/apitechu/v1/users/' + userId) //aqui podríamos usar un .post o un .del o un .put
            .set('x-access-token', token)
            .end(
                function (err, res) {
                    res.should.have.status(200);
                    
                    done();
                }
            )
    }
    )


)

