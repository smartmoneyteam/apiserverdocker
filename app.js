var express = require('express');
var app = express();
var db = require('./db/db.js');

// TODO: meter middleware de seguridad para una clase concreta

//manejador para poder hacer peticiones desde cualquier origen. lo metemos mas abajo como manejador.
var enableCORS = function (req, res, next){
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
    //esto permite recibir la cabecera content-type y cabecera x-access-token para recibir tokens
    res.set('Access-Control-Allow-Headers', 'Content-Type, x-access-token');

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, ' + 
      'Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, x-access-token');
    
    next();
};
  

//si se usa jwt y se usa cabecera se debe permitir el x-
// le pasamos otra capa de middleware. Hay una plitica que impide que se hagan peticiones desde
// una url a otra. esto permite que suceda estas llamadas
app.use(enableCORS);  

var UserController = require('./user/UserController');
app.use('/apitechu/v1/users', UserController);

var AccountController = require('./account/AccountController');
app.use('/apitechu/v1/accounts', AccountController);

var AuthController = require('./auth/AuthController');
app.use('/apitechu/v1/auth', AuthController);

var ForexController = require('./extApi/ForexController');
app.use('/apitechu/v1/stock', ForexController);

module.exports = app;

